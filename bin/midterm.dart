import 'dart:io';
import 'dart:math';

import 'package:midterm/midterm.dart' as midterm;

void main(List<String> arguments) {
  print("Enter mathematical expression:");
  String? mathExpression = stdin.readLineSync();
  List<String> token = create_token(mathExpression);
  print(token); //output ex.1
  List<String> postfix = infix_to_postfix(token);
  print(postfix); //output ex.2
  double answer = evaluate_postfix(postfix);
  print(answer);  //output ex.3
}

//exercise 1
create_token(var me) {
  List<String> token_list = [];
  token_list = me.split(' ');
  int tl_length = token_list.length;
  for (int i = 0; i < tl_length; i++) {
    token_list.remove('');
  }
  return token_list;
}

//exercise 2
infix_to_postfix(var token) {
  List<String> operators = [];
  List<String> postfix = [];
  int precedence, last_precedence = 10;

  for (int i = 0; i < token.length; i++) {
    if (token[i] == "0" ||
        token[i] == "1" ||
        token[i] == "2" ||
        token[i] == "3" ||
        token[i] == "4" ||
        token[i] == "5" ||
        token[i] == "6" ||
        token[i] == "7" ||
        token[i] == "8" ||
        token[i] == "9") {
      postfix.add(token[i]);
    }
    if (token[i] == "+" ||
        token[i] == "-" ||
        token[i] == "*" ||
        token[i] == "/" ||
        token[i] == "%" ||
        token[i] == "**") {
      if (token[i] == "(" || token[i] == ")") {
        precedence = 0;
      } else if (token[i] == "+" || token[i] == "-") {
        precedence = 1;
      } else if (token[i] == "*" || token[i] == "/") {
        precedence = 2;
      } else {
        precedence = 3;
      }
      if (operators.length > 0) {
        if (operators.last == "(" || operators.last == ")") {
          last_precedence = 0;
        } else if (operators.last == "+" || operators.last == "-") {
          last_precedence = 1;
        } else if (operators.last == "*" || operators.last == "/") {
          last_precedence = 2;
        } else {
          last_precedence = 3;
        }
      }
      while (operators.isNotEmpty &&
          operators.last != "(" &&
          precedence <= last_precedence) {
        String keep = operators.last;
        operators.removeLast();
        postfix.add(keep);
      }
      operators.add(token[i]);
    }
    if (token[i] == "(") {
      operators.add(token[i]);
    }
    if (token[i] == ")") {
      while (operators.last != "(") {
        String keep = operators.last;
        operators.removeLast();
        postfix.add(keep);
      }
      operators.remove("(");
    }
  }
  while (operators.isNotEmpty) {
    String keep = operators.last;
    operators.removeLast();
    postfix.add(keep);
  }
  return postfix;
}

//exercise 3
evaluate_postfix(var postfix) {
  List<double> values = [];
  double number;
  for (int i = 0; i < postfix.length; i++) {
    if (postfix[i] == "0" ||
        postfix[i] == "1" ||
        postfix[i] == "2" ||
        postfix[i] == "3" ||
        postfix[i] == "4" ||
        postfix[i] == "5" ||
        postfix[i] == "6" ||
        postfix[i] == "7" ||
        postfix[i] == "8" ||
        postfix[i] == "9") {
      switch (postfix[i]) {
        case "0":
          {
            number = 0;
            values.add(number);
          }
          break;
        case "1":
          {
            number = 1;
            values.add(number);
          }
          break;
        case "2":
          {
            number = 2;
            values.add(number);
          }
          break;
        case "3":
          {
            number = 3;
            values.add(number);
          }
          break;
        case "4":
          {
            number = 4;
            values.add(number);
          }
          break;
        case "5":
          {
            number = 5;
            values.add(number);
          }
          break;
        case "6":
          {
            number = 6;
            values.add(number);
          }
          break;
        case "7":
          {
            number = 7;
            values.add(number);
          }
          break;
        case "8":
          {
            number = 8;
            values.add(number);
          }
          break;
        case "9":
          {
            number = 9;
            values.add(number);
          }
          break;
      }
    } else {
      double right;
      double left;
      right = values.last;
      values.removeLast();
      left = values.last;
      values.removeLast();
      double result = 0; 
      if (postfix[i] == "+") {
        result = (left + right);
      }
      if (postfix[i] == "-") {
        result = (left - right);
      }
      if (postfix[i] == "*") {
        result = (left * right);
      }
      if (postfix[i] == "/") {
        result = (left / right);
      }
      if (postfix[i] == "**") {
        result = (pow(left, right)+ 0.0);
      }
      if (postfix[i] == "%") {
        result = (left % right);
      }
      values.add(result);
    }
  }
  return values[0];
}
 